# Javelin Scripts PreRelease

This repository contains several example scripts which can be used with the Javelin groundstation tools to operate a satellite running the Kubos operating system. The example scripts demonstrate how to send GraphQL commands, ftp commands, and delay commands. These scripts are designed to interact with the built in Kubos services, but any additional hardware services that you build yourself can be controlled in the same manner, simply type the required GraphQL commands into a script file, and pass the file as an argument into the Javelin tools.

For a full description of how to write script files, see the Readme in the Javelin Repository.

## Installation and Usage

To use the scripts in this repository, simply clone the repository into a location close to the Groundstation Tools repository. When running the groundstation tools, use the following command structure:

```
python3 send_cmd_script.py path-to-script
```

The path-to-script argument needs to be the path to the script file you want to run. If that script file is in this repository, then the path must lead to the relevant script file in this repository. Otherwise, you can run script files stored anywhere on your computer.
